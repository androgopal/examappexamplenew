package com.example.newexampanel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.viewpager.widget.PagerAdapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newexampanel.databinding.ActivityMainBinding;
import com.example.newexampanel.databinding.ItemRowQuestionLayoutBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    RecyclerView questions_view;
    TextView tvquestion_no, total_q, time, previous, next, submittest, clear, txt_title;
    LinearLayout preSelectedB;
    JSONObject language_question;
    PagerAdapter adapter;
    JSONArray jsonArray = new JSONArray();
    JSONArray question_list = new JSONArray();
    JSONArray exam_array = new JSONArray();
    JSONObject examObject;
    String examdata1 = "", tlq_option_attach1 = "", tlq_option_attach2 = "", examTime = "";
    int tlq_option_attach = 0;
    private int quesId;
    private GridView questionListGv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        tvquestion_no = findViewById(R.id.tvquestion_no);
        total_q = findViewById(R.id.total_q);
        time = findViewById(R.id.time);
        previous = findViewById(R.id.previous);
        next = findViewById(R.id.next);
        submittest = findViewById(R.id.submittest);
        clear = findViewById(R.id.clear);
        txt_title = findViewById(R.id.txt_title);
        addItemsFromJSON();
        setClickListeners();
        startTimer();
        quesId = 0;
        binding.imgMenu.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                if (!binding.drawelayout.isDrawerOpen(GravityCompat.END))
                    binding.drawelayout.openDrawer(Gravity.END);
                else binding.drawelayout.closeDrawer(Gravity.END);
            }
        });
    }

/*    @SuppressLint("LongLogTag")
    private void addItemsFromJSON() {
        try {
            String jsonDataString = readJSONDataFromFile();
            JSONObject jsonObject = new JSONObject(jsonDataString);
            JSONObject exam_data = jsonObject.getJSONObject("rowExamInfo");
            binding.totalQ.setText("Total Q:" + " " + exam_data.getString("test_total_question"));
            //binding.time.setText(exam_data.getString("test_duration"));

            jsonArray = exam_data.getJSONArray("ot_user_test_subject_view");
            for (int i = 0; i < jsonArray.length(); i++) {
                examObject = jsonArray.getJSONObject(i);
                binding.txtTitle.setText(examObject.getString("sub_name"));
                binding.totalMarks.setText("+" + examObject.getString("tsd_total_marks"));
                binding.negativeMarks.setText("-" + examObject.getString("tsd_negative_marks"));
                question_list = examObject.getJSONArray("ot_question");

                for (int n = 0; n < question_list.length(); n++) {
                    JSONObject jobject = question_list.getJSONObject(n);
                    if (jobject.has("ot_test_language_question")) {
                        language_question = jobject.getJSONObject("ot_test_language_question");
//                       exam_question= language_question.getString("tlq_question_text");
//                         q_solution=language_question.getString("tlq_hindi_solution");
//                        data = language_question.getString("tlq_options");
//                       items = new ArrayList<String>(Arrays.asList(data.split("###OPT###")));
                    }
                }

            }
            LinearLayoutManager layoutManagerL = new LinearLayoutManager(getApplicationContext());
            layoutManagerL.setOrientation(LinearLayoutManager.VERTICAL);
            binding.questionsView.setLayoutManager(layoutManagerL);
            QuestionAdapter questionAdapter = new QuestionAdapter(getApplicationContext(), question_list);
            binding.questionsView.setAdapter(questionAdapter);

        } catch (JSONException | IOException e) {
            ///   Log.d(TAG, "addItemsFromJSON: ", e);
        }
    }*/

    private void addItemsFromJSON() {
        try {
            String jsonDataString = readJSONDataFromFile();
            if (!jsonDataString.equals("null")) {
                JSONObject jsonObject = new JSONObject(jsonDataString);
                question_list = jsonObject.getJSONArray("result");
                Log.d("questionList", question_list + "");
                binding.txtTitle.setText(jsonObject.getString("sub_name"));
                binding.totalQ.setText("Total Q: " + String.valueOf(question_list.length()));
                binding.totalMarks.setText("+" + jsonObject.getString("tsd_total_marks"));
                binding.negativeMarks.setText("+" + jsonObject.getString("tsd_negative_marks"));
                examTime = jsonObject.getString("test_duration");

                LinearLayoutManager layoutManagerL = new LinearLayoutManager(getApplicationContext());
                layoutManagerL.setOrientation(LinearLayoutManager.HORIZONTAL);
                binding.questionsView.setLayoutManager(layoutManagerL);
                QuestionAdapter questionAdapter = new QuestionAdapter(getApplicationContext(), question_list);
                binding.questionsView.setAdapter(questionAdapter);
                setSnapHelper();
            } else {
                Toast.makeText(this, "Data not Found", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String readJSONDataFromFile() throws IOException {
        InputStream inputStream = null;
        StringBuilder builder = new StringBuilder();

        try {

            String jsonString = null;
            inputStream = getResources().openRawResource(R.raw.simple_data);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
                Log.d("fjfjfjfffjfjf", jsonString + "");
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }

    private void setSnapHelper() {
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(binding.questionsView);

        binding.questionsView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                View view = snapHelper.findSnapView(recyclerView.getLayoutManager());
                quesId = recyclerView.getLayoutManager().getPosition(view);
                binding.tvquestionNo.setText("Q." + String.valueOf(quesId + 1));
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }


    private void setClickListeners() {
        
        binding.previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quesId > 0) {
                    binding.questionsView.smoothScrollToPosition(quesId - 1);
                }
            }
        });
        binding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quesId < question_list.length() - 1) {
                    binding.questionsView.smoothScrollToPosition(quesId + 1);
                }
            }
        });
    }

    private void startTimer() {
        long totalTime = Long.parseLong(examTime) * 60 * 1000;
        CountDownTimer timer = new CountDownTimer(totalTime + 1000, 1000) {
            @Override
            public void onTick(long remainingTime) {
                String time = String.format("%02d:%02d min",
                        TimeUnit.MILLISECONDS.toMinutes(remainingTime),
                        TimeUnit.MILLISECONDS.toSeconds(remainingTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime))
                );
                binding.time.setText(time);
            }

            @Override
            public void onFinish() {

            }
        };
        timer.start();
    }


    /*public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private Context context;
        JSONArray item;
        private int row_index;
        private static final int TYPE_HEADER = 0;
        private static final int TYPE_FOOTER = 1;
        private static final int TYPE_ITEM = 2;
        ArrayList<String> items;

        public RecyclerAdapter1(Context context, JSONArray item) {
            this.context = context;
            this.item = item;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == TYPE_ITEM) {
                //Inflating recycle view item layout
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ques, parent, false);
                return new RecyclerAdapter1.ItemViewHolder(itemView);
            } else if (viewType == TYPE_HEADER) {
                //Inflating header view
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_header, parent, false);
                return new RecyclerAdapter1.HeaderViewHolder(itemView);
            } else if (viewType == TYPE_FOOTER) {
                //Inflating footer view
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent, false);
                return new RecyclerAdapter1.FooterViewHolder(itemView);
            } else return null;

        }

        @SuppressLint({"LongLogTag", "SetJavaScriptEnabled"})
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            try {
                JSONObject jsonObject = item.getJSONObject(position);

                if (holder instanceof RecyclerAdapter1.HeaderViewHolder) {
                    RecyclerAdapter1.HeaderViewHolder headerHolder = (RecyclerAdapter1.HeaderViewHolder) holder;
                    headerHolder.headerTitle.setText(jsonObject.getString("question"));

                } else if (holder instanceof RecyclerAdapter1.FooterViewHolder) {
                    RecyclerAdapter1.FooterViewHolder footerHolder = (RecyclerAdapter1.FooterViewHolder) holder;
                    footerHolder.footerText.setText(jsonObject.getString("correct_option"));

                } else if (holder instanceof RecyclerAdapter1.ItemViewHolder) {
                    RecyclerAdapter1.ItemViewHolder itemViewHolder = (RecyclerAdapter1.ItemViewHolder) holder;
                    String data = jsonObject.getString("tlq_options");
                    items = new ArrayList<String>(Arrays.asList(data.split("###OPT###")));
                    itemViewHolder.txt_optionA.setText(items.get(position - 1));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == items.size() + 1) {
                return TYPE_FOOTER;
            }
            return TYPE_ITEM;
        }

        @Override
        public int getItemCount() {
            return item.length() ;
        }

        private class HeaderViewHolder extends RecyclerView.ViewHolder {
            TextView headerTitle;
            WebView header_webview;

            public HeaderViewHolder(View view) {
                super(view);
                headerTitle = (TextView) view.findViewById(R.id.txt_headerTitle);
               // header_webview = (WebView) view.findViewById(R.id.header_webview);
            }
        }

        private class FooterViewHolder extends RecyclerView.ViewHolder {
            TextView footerText;

            public FooterViewHolder(View view) {
                super(view);
                footerText = (TextView) view.findViewById(R.id.footer_text);
            }
        }

        private class ItemViewHolder extends RecyclerView.ViewHolder {
            TextView tvindexA, txt_optionA, tvindexB,txt_optionB,tvindexC,txt_optionC,tvindexD,txt_optionD;
            CardView ll_option, ll_option1;
            LinearLayout ll_optionA,ll_indexA,ll_optionB,ll_indexB,ll_optionC,ll_indexC,ll_optionD,ll_indexD;

            public ItemViewHolder(View itemView) {
                super(itemView);
                tvindexA = itemView.findViewById(R.id.tvindexA);
//                tvindexB = itemView.findViewById(R.id.tvindexB);
//                tvindexC = itemView.findViewById(R.id.tvindexC);
//                tvindexD = itemView.findViewById(R.id.tvindexD);
                txt_optionA = itemView.findViewById(R.id.txt_optionA);
//                txt_optionB = itemView.findViewById(R.id.txt_optionB);
//                txt_optionC = itemView.findViewById(R.id.txt_optionC);
//                txt_optionD = itemView.findViewById(R.id.txt_optionD);
                ll_optionA = (LinearLayout) itemView.findViewById(R.id.ll_optionA);
//                ll_optionB = (LinearLayout) itemView.findViewById(R.id.ll_optionB);
//                ll_optionC = (LinearLayout) itemView.findViewById(R.id.ll_optionC);
//                ll_optionD = (LinearLayout) itemView.findViewById(R.id.ll_optionD);
                ll_indexA = (LinearLayout) itemView.findViewById(R.id.ll_indexA);
//                ll_indexB = (LinearLayout) itemView.findViewById(R.id.ll_indexB);
//                ll_indexC = (LinearLayout) itemView.findViewById(R.id.ll_indexC);
//                ll_indexD = (LinearLayout) itemView.findViewById(R.id.ll_indexD);
            }
        }
    }*/


    public class QuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ItemRowQuestionLayoutBinding binding;
        Context context;
        JSONArray listItem;


        public QuestionAdapter(Context context, JSONArray listItem) {
            this.context = context;
            this.listItem = listItem;
        }

        @NonNull
        @Override
        public QuestionAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            binding = ItemRowQuestionLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            QuestionAdapter.RecyclerViewHolder holder = new QuestionAdapter.RecyclerViewHolder(binding);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
            try {

                JSONObject item = listItem.getJSONObject(i);
                setdata(item, i);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);
            /*holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject item = listItem.getJSONObject(i);
                        Intent intent = new Intent(context.getApplicationContext(), CoursesDetail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("course", item + "");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });*/
        }

        @Override
        public int getItemCount() {
            return listItem.length();
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder {
            public RecyclerViewHolder(@NonNull ItemRowQuestionLayoutBinding itemView) {
                super(itemView.getRoot());


                preSelectedB = null;
            }
        }

        private void setdata(JSONObject item, final int pos) {
            try {
                binding.txtOptionA.setText(item.getString("OptionA"));
                binding.txtOptionB.setText(item.getString("OptionB"));
                binding.txtOptionC.setText(item.getString("OptionC"));
                binding.txtOptionD.setText(item.getString("OptionD"));
                binding.txtHeaderTitle.setText(item.getString("question"));
                /*binding.footerText.setText(item.getString("answer"));
                binding.tvAns.setBackgroundResource(R.drawable.green_circle);
                binding.tvAns.setText(item.getString("correct_option"));*/
            } catch (JSONException e) {
                e.printStackTrace();
            }

           /* for (int i = 0; i < 4; i++) {
                int finalI = i;
                binding.optionsLayout.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    binding.optionsLayout.getChildAt(finalI).setBackgroundResource(R.drawable.exam_layout_background);
                    }
                });*/
            binding.llOptionA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //selectOption(binding.txtOptionA,binding.llIndexA, binding.llOptionA, 1, pos);
                    binding.llOptionA.setBackgroundResource(R.drawable.exam_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                }
            });
            binding.llOptionB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //selectOption(binding.txtOptionB,binding.llIndexB, binding.llOptionB,2, pos);
                    binding.llOptionA.setBackgroundResource(R.drawable.exam_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                }
            });
            binding.llOptionC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //selectOption(binding.txtOptionC,binding.llIndexC , binding.llOptionC, 3, pos);
                    binding.llOptionA.setBackgroundResource(R.drawable.exam_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                }
            });
            binding.llOptionD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //selectOption(binding.txtOptionD,binding.llIndexD , binding.llOptionD, 4, pos);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.exam_layout_background);
                }
            });
        }

    }

       /* private void selectOption(TextView btn, LinearLayout optback, LinearLayout cardView, int option_num, int quesId) {
            if (preSelectedB == null) {
                cardView.setBackgroundResource(R.drawable.correct_answer_layout);
                optback.setBackgroundResource(R.drawable.green_circle);
                preSelectedB = cardView;
                //question_list.get(quesId).
                try {
                    question_list.getJSONObject(option_num);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                if (preSelectedB==cardView){
                    cardView.setBackgroundResource(R.drawable.white_layout_background);
                    optback.setBackgroundResource(R.drawable.grey_circle);
                }else {
                    preSelectedB.setBackgroundResource(R.drawable.white_layout_background);
                    optback.setBackgroundResource(R.drawable.grey_circle);
                    preSelectedB = cardView;
                }
            }
        }*/

}
