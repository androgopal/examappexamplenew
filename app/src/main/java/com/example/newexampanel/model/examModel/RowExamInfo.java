package com.example.newexampanel.model.examModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RowExamInfo{

	@SerializedName("test_admin_id")
	private int testAdminId;

	@SerializedName("test_duration")
	private String testDuration;

	@SerializedName("test_free_status")
	private boolean testFreeStatus;

	@SerializedName("test_dpt_id")
	private int testDptId;

	@SerializedName("test_solution_file")
	private String testSolutionFile;

	@SerializedName("test_datetime")
	private String testDatetime;

	@SerializedName("test_total_marks")
	private int testTotalMarks;

	@SerializedName("product_in_stock")
	private int productInStock;

	@SerializedName("test_marks")
	private int testMarks;

	@SerializedName("test_series_id")
	private int testSeriesId;

	@SerializedName("test_title")
	private String testTitle;

	@SerializedName("test_ts_id")
	private int testTsId;

	@SerializedName("test_section_type")
	private int testSectionType;

	@SerializedName("test_et_id")
	private int testEtId;

	@SerializedName("test_paper_file")
	private String testPaperFile;

	@SerializedName("test_sub_question")
	private String testSubQuestion;

	@SerializedName("test_id")
	private int testId;

	@SerializedName("test_sectional_status")
	private int testSectionalStatus;

	@SerializedName("test_status")
	private int testStatus;

	@SerializedName("test_total_question")
	private int testTotalQuestion;

	@SerializedName("test_start_date")
	private String testStartDate;

	@SerializedName("ot_test_solution_datetime")
	private String otTestSolutionDatetime;

	@SerializedName("test_subject_stope_time")
	private String testSubjectStopeTime;

	@SerializedName("test_live_status")
	private int testLiveStatus;

	@SerializedName("test_sub_id")
	private String testSubId;

	@SerializedName("test_comming_date")
	private String testCommingDate;

	@SerializedName("test_type")
	private int testType;

	@SerializedName("test_end_date")
	private String testEndDate;

	@SerializedName("test_negative_marks")
	private double testNegativeMarks;

	@SerializedName("test_syllabus_info")
	private String testSyllabusInfo;

	@SerializedName("ot_user_test_subject_view")
	private List<OtUserTestSubjectViewItem> otUserTestSubjectView;

	@SerializedName("product_publish_date")
	private String productPublishDate;

	@SerializedName("test_number")
	private int testNumber;

	public void setTestAdminId(int testAdminId){
		this.testAdminId = testAdminId;
	}

	public int getTestAdminId(){
		return testAdminId;
	}

	public void setTestDuration(String testDuration){
		this.testDuration = testDuration;
	}

	public String getTestDuration(){
		return testDuration;
	}

	public void setTestFreeStatus(boolean testFreeStatus){
		this.testFreeStatus = testFreeStatus;
	}

	public boolean isTestFreeStatus(){
		return testFreeStatus;
	}

	public void setTestDptId(int testDptId){
		this.testDptId = testDptId;
	}

	public int getTestDptId(){
		return testDptId;
	}

	public void setTestSolutionFile(String testSolutionFile){
		this.testSolutionFile = testSolutionFile;
	}

	public String getTestSolutionFile(){
		return testSolutionFile;
	}

	public void setTestDatetime(String testDatetime){
		this.testDatetime = testDatetime;
	}

	public String getTestDatetime(){
		return testDatetime;
	}

	public void setTestTotalMarks(int testTotalMarks){
		this.testTotalMarks = testTotalMarks;
	}

	public int getTestTotalMarks(){
		return testTotalMarks;
	}

	public void setProductInStock(int productInStock){
		this.productInStock = productInStock;
	}

	public int getProductInStock(){
		return productInStock;
	}

	public void setTestMarks(int testMarks){
		this.testMarks = testMarks;
	}

	public int getTestMarks(){
		return testMarks;
	}

	public void setTestSeriesId(int testSeriesId){
		this.testSeriesId = testSeriesId;
	}

	public int getTestSeriesId(){
		return testSeriesId;
	}

	public void setTestTitle(String testTitle){
		this.testTitle = testTitle;
	}

	public String getTestTitle(){
		return testTitle;
	}

	public void setTestTsId(int testTsId){
		this.testTsId = testTsId;
	}

	public int getTestTsId(){
		return testTsId;
	}

	public void setTestSectionType(int testSectionType){
		this.testSectionType = testSectionType;
	}

	public int getTestSectionType(){
		return testSectionType;
	}

	public void setTestEtId(int testEtId){
		this.testEtId = testEtId;
	}

	public int getTestEtId(){
		return testEtId;
	}

	public void setTestPaperFile(String testPaperFile){
		this.testPaperFile = testPaperFile;
	}

	public String getTestPaperFile(){
		return testPaperFile;
	}

	public void setTestSubQuestion(String testSubQuestion){
		this.testSubQuestion = testSubQuestion;
	}

	public String getTestSubQuestion(){
		return testSubQuestion;
	}

	public void setTestId(int testId){
		this.testId = testId;
	}

	public int getTestId(){
		return testId;
	}

	public void setTestSectionalStatus(int testSectionalStatus){
		this.testSectionalStatus = testSectionalStatus;
	}

	public int getTestSectionalStatus(){
		return testSectionalStatus;
	}

	public void setTestStatus(int testStatus){
		this.testStatus = testStatus;
	}

	public int getTestStatus(){
		return testStatus;
	}

	public void setTestTotalQuestion(int testTotalQuestion){
		this.testTotalQuestion = testTotalQuestion;
	}

	public int getTestTotalQuestion(){
		return testTotalQuestion;
	}

	public void setTestStartDate(String testStartDate){
		this.testStartDate = testStartDate;
	}

	public String getTestStartDate(){
		return testStartDate;
	}

	public void setOtTestSolutionDatetime(String otTestSolutionDatetime){
		this.otTestSolutionDatetime = otTestSolutionDatetime;
	}

	public String getOtTestSolutionDatetime(){
		return otTestSolutionDatetime;
	}

	public void setTestSubjectStopeTime(String testSubjectStopeTime){
		this.testSubjectStopeTime = testSubjectStopeTime;
	}

	public String getTestSubjectStopeTime(){
		return testSubjectStopeTime;
	}

	public void setTestLiveStatus(int testLiveStatus){
		this.testLiveStatus = testLiveStatus;
	}

	public int getTestLiveStatus(){
		return testLiveStatus;
	}

	public void setTestSubId(String testSubId){
		this.testSubId = testSubId;
	}

	public String getTestSubId(){
		return testSubId;
	}

	public void setTestCommingDate(String testCommingDate){
		this.testCommingDate = testCommingDate;
	}

	public String getTestCommingDate(){
		return testCommingDate;
	}

	public void setTestType(int testType){
		this.testType = testType;
	}

	public int getTestType(){
		return testType;
	}

	public void setTestEndDate(String testEndDate){
		this.testEndDate = testEndDate;
	}

	public String getTestEndDate(){
		return testEndDate;
	}

	public void setTestNegativeMarks(double testNegativeMarks){
		this.testNegativeMarks = testNegativeMarks;
	}

	public double getTestNegativeMarks(){
		return testNegativeMarks;
	}

	public void setTestSyllabusInfo(String testSyllabusInfo){
		this.testSyllabusInfo = testSyllabusInfo;
	}

	public String getTestSyllabusInfo(){
		return testSyllabusInfo;
	}

	public void setOtUserTestSubjectView(List<OtUserTestSubjectViewItem> otUserTestSubjectView){
		this.otUserTestSubjectView = otUserTestSubjectView;
	}

	public List<OtUserTestSubjectViewItem> getOtUserTestSubjectView(){
		return otUserTestSubjectView;
	}

	public void setProductPublishDate(String productPublishDate){
		this.productPublishDate = productPublishDate;
	}

	public String getProductPublishDate(){
		return productPublishDate;
	}

	public void setTestNumber(int testNumber){
		this.testNumber = testNumber;
	}

	public int getTestNumber(){
		return testNumber;
	}
}