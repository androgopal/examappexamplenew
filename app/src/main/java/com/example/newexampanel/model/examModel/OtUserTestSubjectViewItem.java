package com.example.newexampanel.model.examModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OtUserTestSubjectViewItem{

	@SerializedName("tsd_id")
	private int tsdId;

	@SerializedName("tsd_total_marks")
	private String tsdTotalMarks;

	@SerializedName("sub_parent_id")
	private int subParentId;

	@SerializedName("tsd_total_question")
	private String tsdTotalQuestion;

	@SerializedName("sub_name")
	private String subName;

	@SerializedName("tsd_negative_marks")
	private double tsdNegativeMarks;

	@SerializedName("sub_status")
	private int subStatus;

	@SerializedName("tsd_subject_id")
	private String tsdSubjectId;

	@SerializedName("sub_id")
	private int subId;

	@SerializedName("ot_question")
	private List<OtQuestionItem> otQuestion;

	@SerializedName("tsd_stop_time")
	private String tsdStopTime;

	@SerializedName("tsd_test_id")
	private int tsdTestId;

	public void setTsdId(int tsdId){
		this.tsdId = tsdId;
	}

	public int getTsdId(){
		return tsdId;
	}

	public void setTsdTotalMarks(String tsdTotalMarks){
		this.tsdTotalMarks = tsdTotalMarks;
	}

	public String getTsdTotalMarks(){
		return tsdTotalMarks;
	}

	public void setSubParentId(int subParentId){
		this.subParentId = subParentId;
	}

	public int getSubParentId(){
		return subParentId;
	}

	public void setTsdTotalQuestion(String tsdTotalQuestion){
		this.tsdTotalQuestion = tsdTotalQuestion;
	}

	public String getTsdTotalQuestion(){
		return tsdTotalQuestion;
	}

	public void setSubName(String subName){
		this.subName = subName;
	}

	public String getSubName(){
		return subName;
	}

	public void setTsdNegativeMarks(double tsdNegativeMarks){
		this.tsdNegativeMarks = tsdNegativeMarks;
	}

	public double getTsdNegativeMarks(){
		return tsdNegativeMarks;
	}

	public void setSubStatus(int subStatus){
		this.subStatus = subStatus;
	}

	public int getSubStatus(){
		return subStatus;
	}

	public void setTsdSubjectId(String tsdSubjectId){
		this.tsdSubjectId = tsdSubjectId;
	}

	public String getTsdSubjectId(){
		return tsdSubjectId;
	}

	public void setSubId(int subId){
		this.subId = subId;
	}

	public int getSubId(){
		return subId;
	}

	public void setOtQuestion(List<OtQuestionItem> otQuestion){
		this.otQuestion = otQuestion;
	}

	public List<OtQuestionItem> getOtQuestion(){
		return otQuestion;
	}

	public void setTsdStopTime(String tsdStopTime){
		this.tsdStopTime = tsdStopTime;
	}

	public String getTsdStopTime(){
		return tsdStopTime;
	}

	public void setTsdTestId(int tsdTestId){
		this.tsdTestId = tsdTestId;
	}

	public int getTsdTestId(){
		return tsdTestId;
	}
}