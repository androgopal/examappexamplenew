package com.example.newexampanel.model.examModel;

import com.google.gson.annotations.SerializedName;

public class ExamResponse{

	@SerializedName("rowUserSectionTest")
	private RowUserSectionTest rowUserSectionTest;

	@SerializedName("rowExamInfo")
	private RowExamInfo rowExamInfo;

	@SerializedName("message")
	private String message;

	public void setRowUserSectionTest(RowUserSectionTest rowUserSectionTest){
		this.rowUserSectionTest = rowUserSectionTest;
	}

	public RowUserSectionTest getRowUserSectionTest(){
		return rowUserSectionTest;
	}

	public void setRowExamInfo(RowExamInfo rowExamInfo){
		this.rowExamInfo = rowExamInfo;
	}

	public RowExamInfo getRowExamInfo(){
		return rowExamInfo;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}