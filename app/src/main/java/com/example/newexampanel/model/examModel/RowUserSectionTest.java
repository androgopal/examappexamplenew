package com.example.newexampanel.model.examModel;

import com.google.gson.annotations.SerializedName;

public class RowUserSectionTest{

	@SerializedName("ust_wrong_question")
	private int ustWrongQuestion;

	@SerializedName("ust_test_id")
	private int ustTestId;

	@SerializedName("ust_remainingtime")
	private String ustRemainingtime;

	@SerializedName("ust_id")
	private int ustId;

	@SerializedName("ust_user_id")
	private int ustUserId;

	@SerializedName("ust_correct_question")
	private int ustCorrectQuestion;

	@SerializedName("ust_question_mark")
	private int ustQuestionMark;

	@SerializedName("ust_dob")
	private Object ustDob;

	@SerializedName("ust_created")
	private String ustCreated;

	@SerializedName("ust_status")
	private int ustStatus;

	public void setUstWrongQuestion(int ustWrongQuestion){
		this.ustWrongQuestion = ustWrongQuestion;
	}

	public int getUstWrongQuestion(){
		return ustWrongQuestion;
	}

	public void setUstTestId(int ustTestId){
		this.ustTestId = ustTestId;
	}

	public int getUstTestId(){
		return ustTestId;
	}

	public void setUstRemainingtime(String ustRemainingtime){
		this.ustRemainingtime = ustRemainingtime;
	}

	public String getUstRemainingtime(){
		return ustRemainingtime;
	}

	public void setUstId(int ustId){
		this.ustId = ustId;
	}

	public int getUstId(){
		return ustId;
	}

	public void setUstUserId(int ustUserId){
		this.ustUserId = ustUserId;
	}

	public int getUstUserId(){
		return ustUserId;
	}

	public void setUstCorrectQuestion(int ustCorrectQuestion){
		this.ustCorrectQuestion = ustCorrectQuestion;
	}

	public int getUstCorrectQuestion(){
		return ustCorrectQuestion;
	}

	public void setUstQuestionMark(int ustQuestionMark){
		this.ustQuestionMark = ustQuestionMark;
	}

	public int getUstQuestionMark(){
		return ustQuestionMark;
	}

	public void setUstDob(Object ustDob){
		this.ustDob = ustDob;
	}

	public Object getUstDob(){
		return ustDob;
	}

	public void setUstCreated(String ustCreated){
		this.ustCreated = ustCreated;
	}

	public String getUstCreated(){
		return ustCreated;
	}

	public void setUstStatus(int ustStatus){
		this.ustStatus = ustStatus;
	}

	public int getUstStatus(){
		return ustStatus;
	}
}