package com.example.newexampanel.model.examModel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OtQuestionItem{

	@SerializedName("que_test_id")
	private String queTestId;

	@SerializedName("que_order")
	private int queOrder;

	@SerializedName("que_correct_option")
	private String queCorrectOption;

	@SerializedName("que_area")
	private int queArea;

	@SerializedName("que_correct_option_hindi")
	private String queCorrectOptionHindi;

	@SerializedName("ot_test_language_question")
	private OtTestLanguageQuestion otTestLanguageQuestion;

	@SerializedName("que_id")
	private int queId;

	@SerializedName("ust_ques_answer")
	private Object ustQuesAnswer;

	@SerializedName("que_sub_id")
	private String queSubId;

	@SerializedName("que_tlq_id")
	private int queTlqId;

	@SerializedName("ot_matchword_ques")
	private List<Object> otMatchwordQues;

	public void setQueTestId(String queTestId){
		this.queTestId = queTestId;
	}

	public String getQueTestId(){
		return queTestId;
	}

	public void setQueOrder(int queOrder){
		this.queOrder = queOrder;
	}

	public int getQueOrder(){
		return queOrder;
	}

	public void setQueCorrectOption(String queCorrectOption){
		this.queCorrectOption = queCorrectOption;
	}

	public String getQueCorrectOption(){
		return queCorrectOption;
	}

	public void setQueArea(int queArea){
		this.queArea = queArea;
	}

	public int getQueArea(){
		return queArea;
	}

	public void setQueCorrectOptionHindi(String queCorrectOptionHindi){
		this.queCorrectOptionHindi = queCorrectOptionHindi;
	}

	public String getQueCorrectOptionHindi(){
		return queCorrectOptionHindi;
	}

	public void setOtTestLanguageQuestion(OtTestLanguageQuestion otTestLanguageQuestion){
		this.otTestLanguageQuestion = otTestLanguageQuestion;
	}

	public OtTestLanguageQuestion getOtTestLanguageQuestion(){
		return otTestLanguageQuestion;
	}

	public void setQueId(int queId){
		this.queId = queId;
	}

	public int getQueId(){
		return queId;
	}

	public void setUstQuesAnswer(Object ustQuesAnswer){
		this.ustQuesAnswer = ustQuesAnswer;
	}

	public Object getUstQuesAnswer(){
		return ustQuesAnswer;
	}

	public void setQueSubId(String queSubId){
		this.queSubId = queSubId;
	}

	public String getQueSubId(){
		return queSubId;
	}

	public void setQueTlqId(int queTlqId){
		this.queTlqId = queTlqId;
	}

	public int getQueTlqId(){
		return queTlqId;
	}

	public void setOtMatchwordQues(List<Object> otMatchwordQues){
		this.otMatchwordQues = otMatchwordQues;
	}

	public List<Object> getOtMatchwordQues(){
		return otMatchwordQues;
	}
}