package com.example.newexampanel;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.example.newexampanel.database.Entity.QuestionTable;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Constant {

    //public static final String YOUR_API_URL_LIVE  = "http://192.168.1.38/coachingzon/cswebservices/";
    public static final String YOUR_API_WEB_LIVE = "http://192.168.1.38/coachingzon/cswebservices/pages/";

    public static final String YOUR_API_URL_LIVE = "https://coachingzon.com/portal/cswebservices/";
    public static final String LOGIN = "login/loginwithpassword";
    public static final String category_main = "dashboard";
    public static final String testseries = "user/testseries";
    public static final String testcat = "user/testcat";
    public static final String syllabus = "user/syllabus";
    public static final String Base_exam = "https://neonclasses.com/webapi/";

    public static final String search = "user/search";

    public static final int NOT_VISITED = 1;
    public static final int UNANSWERED = 2;
    public static final int ANSWERED = 3;
    public static final int REVIEW = 4;
    public static int quePosition =0;


    public static String getCalculatedDate(String date, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date date1 = s.parse(date);
            cal.setTime(date1);
            cal.add(Calendar.DAY_OF_MONTH, days);
            Date newDate = cal.getTime();
            return s.format(newDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void hideError(TextInputEditText editText, final TextInputLayout layout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                layout.setError(null);
                layout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public static GetResponse getUrl() {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(1000, TimeUnit.MILLISECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.YOUR_API_URL_LIVE)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        return retrofit.create(GetResponse.class);
    }

    public static GetResponse newgetUrl() {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(5000, TimeUnit.MILLISECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.Base_exam)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        return retrofit.create(GetResponse.class);
    }

    public static String getCalculatedDateOther(String date, int days) {
        Calendar cal = Calendar.getInstance();

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date date1 = s.parse(date);
            cal.setTime(date1);
            cal.add(Calendar.DAY_OF_MONTH, days);
            Date newDate = cal.getTime();

            SimpleDateFormat new_format = new SimpleDateFormat("dd MMMM", Locale.getDefault());

            return new_format.format(newDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static SimpleDateFormat serverDate() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }

    public static Date changedateformat(String date) throws ParseException {
        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.CANADA);
        return df.parse(date);

    }

    public static ArrayList<String> snack_array = new ArrayList<>();


    public static boolean isNetworkConnectionAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if (isConnected) {
            Log.d("Network", "Connected to internet");
            return true;
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("No internet Connection");
            builder.setMessage("Please turn on internet connection to continue");
            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            return false;
        }
    }

    public static void fullScreen(AppCompatActivity activity) {
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = activity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

    }

    public static void setToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void callBack(final AppCompatActivity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        activity.finishAffinity();
                        System.exit(0);
                    }
                }, 000);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

        Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(Color.parseColor("#D6483F"));

        Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.parseColor("#008000"));

    }

    public static void logPrint(String title, String message) {

        Log.d(title, message + "");

    }

    public static void hideKeyBoard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void setStatusBar(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.white, activity.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.white));
        }
    }

    @SuppressLint("RestrictedApi")
    public static void setToolbar(final AppCompatActivity activity, Toolbar toolbar) {
        activity.setSupportActionBar(toolbar);
        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            activity.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onBackPressed();
                }
            });
            toolbar.getNavigationIcon().setColorFilter(activity.getResources().getColor(R.color.purple_700), PorterDuff.Mode.SRC_ATOP);
            activity.setTitle("");
        }
    }

    public static void printHashKey(AppCompatActivity activity) {
        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(activity.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("TAG", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("TAG", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("TAG", "printHashKey()", e);
        }
    }

    public static ColorStateList getColorState() {

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{

                        Color.parseColor("#666666") //disabled
                        , Color.parseColor("#fdbc0a") //enabled

                }
        );
        return colorStateList;
    }

    public static ColorStateList getColorStatePaytm() {

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{

                        Color.parseColor("#666666") //disabled
                        , Color.parseColor("#00b9f5") //enabled

                }
        );
        return colorStateList;
    }

    public static ProgressDialog getProgressBar(Context context) {
        ProgressDialog loading = new ProgressDialog(context);
        loading.setMessage("Please wait... For a while");
        loading.setIndeterminate(true);
        loading.setCancelable(false);
        loading.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.loader));
        return loading;
    }

    public static Typeface getFonts(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    public static Typeface getsemiFonts(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Semibold.ttf");
    }

    public static Typeface getFontsBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
    }

    public static Typeface getFontsExtraBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-ExtraBold.ttf");
    }

    public static String fcm_token = "";

    public static void printHashKey() {
    }

    public void writeTokenToFile(AppCompatActivity activity, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(activity.openFileOutput("token.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static String readTokenFromFile(AppCompatActivity activity) {
        String ret = "";
        try {
            InputStream inputStream = activity.openFileInput("token.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }


    public static final int MULTIPLE_PERMISSIONS = 100;

    public static boolean checkPermissions(AppCompatActivity activity, String[] permissions) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    public static void writeSkipToFile(AppCompatActivity activity, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(activity.openFileOutput("skip.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static String readSkipFromFile(AppCompatActivity activity) {
        String ret = "no";
        try {
            InputStream inputStream = activity.openFileInput("skip.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }

    public static String getdiscount(String actualprice, String newprice) {
        String discount = "0";
        Double actualpriced = Double.parseDouble(actualprice);
        Double newpriced = Double.parseDouble(newprice);

        Double difference = actualpriced - newpriced;
        logPrint(difference + "", "differencedifferencedifferencedifferencedifferencedifference" + actualpriced);
        if (difference > 0) {
            Double differeperc = (difference / actualpriced) * 100;
            logPrint(differeperc + "", "differepercdifferepercdifferepercdiffereperc");
            discount = differeperc + "";
        }
        return String.format("%.2f", Double.parseDouble(discount));
    }

    public static ImageLoader imageLoader_;

    public static ImageLoaderConfiguration getImageLoaderConfig(Context context) {
        DisplayImageOptions options_dis = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_img)
                .showImageForEmptyUri(R.drawable.no_img)
                .showImageOnFail(R.drawable.no_img)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        return new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options_dis)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
    }

    private static DisplayImageOptions getImageOptions() {
        return new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.noimg)
                .showImageForEmptyUri(R.drawable.noimg)
                .showImageOnFail(R.drawable.noimg)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    public static void setImage(Context context, String url, final ImageView imageView) {

        if (url.equals("")) {
            imageView.setImageResource(R.drawable.no_image);
        } else {

            Glide.with(context)
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.no_image)
                    .into(imageView);

        }

    }

//    public static void get_date(Context context,String date1){
//        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
//        String outputPattern = "dd-MM-yyyy";
//        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
//        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
//
//        Date date = null;
//        String str = null;
//
//        try {
//            date = inputFormat.parse(item.getString("updated_at"));
//            str = outputFormat.format(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

}

