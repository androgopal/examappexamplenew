package com.example.newexampanel.viewmodel.model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.newexampanel.database.Entity.QuestionTable;
import com.example.newexampanel.database.Entity.RawExamInfoTable;
import com.example.newexampanel.database.Entity.SubjectDataTable;
import com.example.newexampanel.model.examModel.ExamResponse;
import com.example.newexampanel.studentexam.ExamActivity;
import com.example.newexampanel.viewmodel.repository.DatabaseRepo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class Viewmodel extends AndroidViewModel {
    private DatabaseRepo repository;
    ExamActivity examData;
    RawExamInfoTable rawExamInfoTable;

    public Viewmodel(@NonNull Application application) {
        super(application);
        repository = new DatabaseRepo(application);
        examData = new ExamActivity();
    }

    public LiveData<RawExamInfoTable> getExamData() {
        return repository.getRawExamInfo();
    }

    public LiveData<List<SubjectDataTable>> getSubjectata(int testid) {
        return repository.getSubjectDetail(testid);
    }

    public LiveData<List<QuestionTable>> getQuestiontata(String subjectId) {
        return repository.getQuestionTable(subjectId);
    }

    public void GetExamDataFromApi(String jsondata) {
        Type type = new TypeToken<ExamResponse>() {
        }.getType();
        Gson gson = new Gson();
        ExamResponse response = gson.fromJson(jsondata, type);
        //Log.e("response123",response.toString());




        repository.Insert(new RawExamInfoTable(response.getRowExamInfo().getTestId(), response.getRowExamInfo().getTestDuration(), response.getRowExamInfo().getTestTsId(),
                response.getRowExamInfo().getTestDptId(), response.getRowExamInfo().getTestEtId(), response.getRowExamInfo().getTestNumber(), response.getRowExamInfo().getTestSubId(),
                response.getRowExamInfo().getTestMarks(), response.getRowExamInfo().getTestNegativeMarks(), response.getRowExamInfo().getTestSubQuestion(),
                response.getRowExamInfo().getTestDatetime(), response.getRowExamInfo().getTestStatus(), response.getRowExamInfo().getTestAdminId(), response.getRowExamInfo().getTestSeriesId(),
                response.getRowExamInfo().getTestPaperFile(), response.getRowExamInfo().getTestSolutionFile(), response.getRowExamInfo().isTestFreeStatus(),
                response.getRowExamInfo().getProductInStock(), response.getRowExamInfo().getTestSectionalStatus(), response.getRowExamInfo().getTestLiveStatus(),
                response.getRowExamInfo().getTestEndDate(), response.getRowExamInfo().getTestStartDate(), response.getRowExamInfo().getOtTestSolutionDatetime(),
                response.getRowExamInfo().getTestSubjectStopeTime(), response.getRowExamInfo().getTestSyllabusInfo(), response.getRowExamInfo().getTestCommingDate(),
                response.getRowExamInfo().getTestTitle(), response.getRowExamInfo().getTestTotalMarks(), response.getRowExamInfo().getTestTotalQuestion(),
                response.getRowExamInfo().getProductPublishDate(), response.getRowExamInfo().getTestSectionType(), response.getRowExamInfo().getTestType()));

        for (int i = 0; i < response.getRowExamInfo().getOtUserTestSubjectView().size(); i++) {
            repository.Insert(new SubjectDataTable(response.getRowExamInfo().getTestId(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdSubjectId(),
                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdNegativeMarks(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdStopTime(),
                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTestId(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubId(),
                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubStatus(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubParentId(),
                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTotalQuestion(),response.getRowExamInfo().getOtUserTestSubjectView().get(i).getTsdTotalMarks(),
                    response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubName()));

            for (int j=0; j <response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().size(); j++){
                repository.Insert(new QuestionTable(response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqId(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionId(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqUniqueId(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionType(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptions(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption1Attachments(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption2Attachments(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption3Attachments(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption4Attachments(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption5Attachments(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqLangId(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionText(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionTextHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionsHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionAttachments(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption1AttachmentsHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption2AttachmentsHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption3AttachmentsHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption4AttachmentsHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOption5AttachmentsHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().isTlqQuestionTypeHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionAttachmentsHindi(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishInstruction(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiInstruction(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiSolution(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishSolution(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqHindiSolutionAttach(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqEnglishSolutionAttach(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionAttach(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().isTlqQuestionStatus(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqQuestionOrder(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqUserId(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqOptionHindiAttach(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getSubId(),
                        response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqCorrectOption()));
                Log.d("ldldldll",response.getRowExamInfo().getOtUserTestSubjectView().size()+"++"+i+"++"+response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().size()
                +"+++"+j+"+++"+response.getRowExamInfo().getOtUserTestSubjectView().get(i).getOtQuestion().get(j).getOtTestLanguageQuestion().getTlqCorrectOption());

            }
        }

        /*for (int i = 0; i < response.getRowExamInfo().getOtUserTestSubjectView().size(); i++) {
            repository.Insert(new SubjectDataTable());
        }
        repository.Insert(new QuestionTable());*/
        //Call api here
        //Insert data into database
        //from repository.insert(rawExaminfoObject)
    }

    public void DeleteAll() {
        repository.DeleteAll();
    }

    public void updateQuestionTable(QuestionTable clickedItem) {
        repository.Insert(clickedItem);
    }
}
