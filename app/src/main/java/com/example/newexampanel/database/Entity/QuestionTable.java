package com.example.newexampanel.database.Entity;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class QuestionTable {

    @PrimaryKey()
    private int tlqId;
    @ColumnInfo(name ="tlq_question_id")
    private String questionId;
    @ColumnInfo(name ="tlq_unique_id")
    private String uniqueId;
    @ColumnInfo(name ="tlq_question_type")
    private int questionType;
    @ColumnInfo(name ="tlq_options")
    private String OptionData;
    @ColumnInfo(name ="tlq_option1_attachments")
    private String option1Attach;
    @ColumnInfo(name ="tlq_option2_attachments")
    private String option2Attach;
    @ColumnInfo(name ="tlq_option3_attachments")
    private String option3Attach;
    @ColumnInfo(name ="tlq_option4_attachments")
    private String option4Attach;
    @ColumnInfo(name ="tlq_option5_attachments")
    private String option5Attach;
    @ColumnInfo(name ="tlq_lang_id")
    private String languageId;
    @ColumnInfo(name ="tlq_question_text")
    private String questionHeader;
    @ColumnInfo(name ="tlq_question_text_hindi")
    private String questionHeaderHindi;
    @ColumnInfo(name ="tlq_options_hindi")
    private String optionDataHindi;
    @ColumnInfo(name ="tlq_question_attachments")
    private String questionAttachment;
    @ColumnInfo(name ="tlq_option1_attachments_hindi")
    private String option1AttachmentHindi;
    @ColumnInfo(name ="tlq_option2_attachments_hindi")
    private String option2AttachmentHindi;
    @ColumnInfo(name ="tlq_option3_attachments_hindi")
    private String option3AttachmentHindi;
    @ColumnInfo(name ="tlq_option4_attachments_hindi")
    private String option4AttachmentHindi;
    @ColumnInfo(name ="tlq_option5_attachments_hindi")
    private String option5AttachmentHindi;
    @ColumnInfo(name ="tlq_question_type_hindi")
    private boolean questionTypeHindi;
    @ColumnInfo(name ="tlq_question_attachments_hindi")
    private String questionAttachmentHindi;
    @ColumnInfo(name ="tlq_english_instruction")
    private String englishInstruction;
    @ColumnInfo(name ="tlq_hindi_instruction")
    private String hindiInstruction;
    @ColumnInfo(name ="tlq_hindi_solution")
    private String hindiSolution;
    @ColumnInfo(name ="tlq_english_solution")
    private String englishSolution;
    @ColumnInfo(name ="tlq_hindi_solution_attach")
    private String hindiSolutionAttach;
    @ColumnInfo(name ="tlq_english_solution_attach")
    private String englishSolutionAttach;
    @ColumnInfo(name ="tlq_option_attach")
    private String optionAttach;
    @ColumnInfo(name ="tlq_question_status")
    private boolean questionStatus;
    @ColumnInfo(name ="tlq_question_order")
    private int questionOrder;
    @ColumnInfo(name ="tlq_user_id")
    private int userId;
    @ColumnInfo(name ="tlq_option_hindi_attach")
    private String optionHindiAttach;
    @ColumnInfo(name ="tlq_sub_id")
    private int subjectId;
    @ColumnInfo(name ="tlq_correct_option")
    private int correctOption;

    @ColumnInfo(name ="UserSelectedAns")
    private int UserSelectedAns=0;
    @ColumnInfo(name ="user_que_status")
    private int userQueStatus=0;
    @ColumnInfo(name ="user_que_review")
    private boolean userQueReview;

    public int getUserQueStatus() {
        return userQueStatus;
    }

    public void setUserQueStatus(int userQueStatus) {
        this.userQueStatus = userQueStatus;
    }

    public int getUserSelectedAns() {
        return UserSelectedAns;
    }

    public void setUserSelectedAns(int userSelectedAns) {
        UserSelectedAns = userSelectedAns;
    }

    public boolean isUserQueReview() {
        return userQueReview;
    }

    public void setUserQueReview(boolean userQueReview) {
        this.userQueReview = userQueReview;
    }

    public QuestionTable(int tlqId, String questionId, String uniqueId, int questionType, String OptionData, String option1Attach, String option2Attach, String option3Attach, String option4Attach,
                         String option5Attach, String languageId, String questionHeader, String questionHeaderHindi, String optionDataHindi, String questionAttachment, String option1AttachmentHindi,
                         String option2AttachmentHindi, String option3AttachmentHindi, String option4AttachmentHindi, String option5AttachmentHindi, boolean questionTypeHindi,
                         String questionAttachmentHindi, String englishInstruction, String hindiInstruction, String hindiSolution, String englishSolution, String hindiSolutionAttach,
                         String englishSolutionAttach, String optionAttach, boolean questionStatus, int questionOrder, int userId, String optionHindiAttach, int subjectId,
                         int correctOption) {
        this.tlqId = tlqId;
        this.questionId = questionId;
        this.uniqueId = uniqueId;
        this.questionType = questionType;
        this.OptionData = OptionData;
        this.option1Attach = option1Attach;
        this.option2Attach = option2Attach;
        this.option3Attach = option3Attach;
        this.option4Attach = option4Attach;
        this.option5Attach = option5Attach;
        this.languageId = languageId;
        this.questionHeader = questionHeader;
        this.questionHeaderHindi = questionHeaderHindi;
        this.optionDataHindi = optionDataHindi;
        this.questionAttachment = questionAttachment;
        this.option1AttachmentHindi = option1AttachmentHindi;
        this.option2AttachmentHindi = option2AttachmentHindi;
        this.option3AttachmentHindi = option3AttachmentHindi;
        this.option4AttachmentHindi = option4AttachmentHindi;
        this.option5AttachmentHindi = option5AttachmentHindi;
        this.questionTypeHindi = questionTypeHindi;
        this.questionAttachmentHindi = questionAttachmentHindi;
        this.englishInstruction = englishInstruction;
        this.hindiInstruction = hindiInstruction;
        this.hindiSolution = hindiSolution;
        this.englishSolution = englishSolution;
        this.hindiSolutionAttach = hindiSolutionAttach;
        this.englishSolutionAttach = englishSolutionAttach;
        this.optionAttach = optionAttach;
        this.questionStatus = questionStatus;
        this.questionOrder = questionOrder;
        this.userId = userId;
        this.optionHindiAttach = optionHindiAttach;
        this.subjectId = subjectId;
        this.correctOption = correctOption;
    }


    public  QuestionTable(){

    }

    public String getQuestionAttachment() {
        return questionAttachment;
    }

    public void setQuestionAttachment(String questionAttachment) {
        this.questionAttachment = questionAttachment;
    }

    public int getTlqId() {
        return tlqId;
    }

    public void setTlqId(int tlqId) {
        this.tlqId = tlqId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getQuestionType() {
        return questionType;
    }

    public void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public String getOptionData() {
        return OptionData;
    }

    public void setOptionData(String optionData) {
        OptionData = optionData;
    }

    public String getOption1Attach() {
        return option1Attach;
    }

    public void setOption1Attach(String option1Attach) {
        this.option1Attach = option1Attach;
    }

    public String getOption2Attach() {
        return option2Attach;
    }

    public void setOption2Attach(String option2Attach) {
        this.option2Attach = option2Attach;
    }

    public String getOption3Attach() {
        return option3Attach;
    }

    public void setOption3Attach(String option3Attach) {
        this.option3Attach = option3Attach;
    }

    public String getOption4Attach() {
        return option4Attach;
    }

    public void setOption4Attach(String option4Attach) {
        this.option4Attach = option4Attach;
    }

    public String getOption5Attach() {
        return option5Attach;
    }

    public void setOption5Attach(String option5Attach) {
        this.option5Attach = option5Attach;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getQuestionHeader() {
        return questionHeader;
    }

    public void setQuestionHeader(String questionHeader) {
        this.questionHeader = questionHeader;
    }

    public String getQuestionHeaderHindi() {
        return questionHeaderHindi;
    }

    public void setQuestionHeaderHindi(String questionHeaderHindi) {
        this.questionHeaderHindi = questionHeaderHindi;
    }

    public String getOptionDataHindi() {
        return optionDataHindi;
    }

    public void setOptionDataHindi(String optionDataHindi) {
        this.optionDataHindi = optionDataHindi;
    }

    public String getQuestionAttachmentHindi() {
        return questionAttachmentHindi;
    }

    public void setQuestionAttachmentHindi(String questionAttachmentHindi) {
        this.questionAttachmentHindi = questionAttachmentHindi;
    }

    public String getEnglishInstruction() {
        return englishInstruction;
    }

    public void setEnglishInstruction(String englishInstruction) {
        this.englishInstruction = englishInstruction;
    }

    public String getHindiInstruction() {
        return hindiInstruction;
    }

    public void setHindiInstruction(String hindiInstruction) {
        this.hindiInstruction = hindiInstruction;
    }

    public String getHindiSolution() {
        return hindiSolution;
    }

    public void setHindiSolution(String hindiSolution) {
        this.hindiSolution = hindiSolution;
    }

    public String getEnglishSolution() {
        return englishSolution;
    }

    public void setEnglishSolution(String englishSolution) {
        this.englishSolution = englishSolution;
    }

    public String getHindiSolutionAttach() {
        return hindiSolutionAttach;
    }

    public void setHindiSolutionAttach(String hindiSolutionAttach) {
        this.hindiSolutionAttach = hindiSolutionAttach;
    }

    public String getEnglishSolutionAttach() {
        return englishSolutionAttach;
    }

    public void setEnglishSolutionAttach(String englishSolutionAttach) {
        this.englishSolutionAttach = englishSolutionAttach;
    }

    public String getOptionAttach() {
        return optionAttach;
    }

    public void setOptionAttach(String optionAttach) {
        this.optionAttach = optionAttach;
    }

    public boolean isQuestionStatus() {
        return questionStatus;
    }

    public void setQuestionStatus(boolean questionStatus) {
        this.questionStatus = questionStatus;
    }

    public int getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(int questionOrder) {
        this.questionOrder = questionOrder;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getOptionHindiAttach() {
        return optionHindiAttach;
    }

    public void setOptionHindiAttach(String optionHindiAttach) {
        this.optionHindiAttach = optionHindiAttach;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public int getCorrectOption() {
        return correctOption;
    }

    public void setCorrectOption(int correctOption) {
        this.correctOption = correctOption;
    }

    public String getOption1AttachmentHindi() {
        return option1AttachmentHindi;
    }

    public void setOption1AttachmentHindi(String option1AttachmentHindi) {
        this.option1AttachmentHindi = option1AttachmentHindi;
    }

    public String getOption2AttachmentHindi() {
        return option2AttachmentHindi;
    }

    public void setOption2AttachmentHindi(String option2AttachmentHindi) {
        this.option2AttachmentHindi = option2AttachmentHindi;
    }

    public String getOption3AttachmentHindi() {
        return option3AttachmentHindi;
    }

    public void setOption3AttachmentHindi(String option3AttachmentHindi) {
        this.option3AttachmentHindi = option3AttachmentHindi;
    }

    public String getOption4AttachmentHindi() {
        return option4AttachmentHindi;
    }

    public void setOption4AttachmentHindi(String option4AttachmentHindi) {
        this.option4AttachmentHindi = option4AttachmentHindi;
    }

    public String getOption5AttachmentHindi() {
        return option5AttachmentHindi;
    }

    public void setOption5AttachmentHindi(String option5AttachmentHindi) {
        this.option5AttachmentHindi = option5AttachmentHindi;
    }

    public boolean isQuestionTypeHindi() {
        return questionTypeHindi;
    }

    public void setQuestionTypeHindi(boolean questionTypeHindi) {
        this.questionTypeHindi = questionTypeHindi;
    }
}
