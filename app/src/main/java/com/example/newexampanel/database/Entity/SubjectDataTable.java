package com.example.newexampanel.database.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class SubjectDataTable {
    @PrimaryKey
    private int testId;
    @ColumnInfo(name ="tsd_subject_id")
    private String testDuration;
    @ColumnInfo(name ="tsd_negative_marks")
    private double testNeagtiveMarks;
    @ColumnInfo(name ="tsd_stop_time")
    private String stopTime;
    @ColumnInfo(name ="tsd_test_id")
    private int testNumber;
    @ColumnInfo(name ="sub_id")
    private int SubjectId;
    @ColumnInfo(name ="sub_status")
    private int subjectStatus;
    @ColumnInfo(name ="sub_parent_id")
    private int subjectParentId;
    @ColumnInfo(name ="tsd_total_question")
    private String totalQuestion;
    @ColumnInfo(name ="tsd_total_marks")
    private String testTotalMarks;
    @ColumnInfo(name ="sub_name")
    private String subjectName;


    public SubjectDataTable(int testId, String testDuration, double testNeagtiveMarks, String stopTime, int testNumber, int subjectId, int subjectStatus, int subjectParentId,
                            String totalQuestion, String testTotalMarks, String subjectName) {
        this.testId = testId;
        this.testDuration = testDuration;
        this.testNeagtiveMarks = testNeagtiveMarks;
        this.stopTime = stopTime;
        this.testNumber = testNumber;
        SubjectId = subjectId;
        this.subjectStatus = subjectStatus;
        this.subjectParentId = subjectParentId;
        this.totalQuestion = totalQuestion;
        this.testTotalMarks = testTotalMarks;
        this.subjectName = subjectName;
    }
    public SubjectDataTable() {

    }


    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public String getTestDuration() {
        return testDuration;
    }

    public void setTestDuration(String testDuration) {
        this.testDuration = testDuration;
    }

    public double getTestNeagtiveMarks() {
        return testNeagtiveMarks;
    }

    public void setTestNeagtiveMarks(double testNeagtiveMarks) {
        this.testNeagtiveMarks = testNeagtiveMarks;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public int getTestNumber() {
        return testNumber;
    }

    public void setTestNumber(int testNumber) {
        this.testNumber = testNumber;
    }

    public int getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(int subjectId) {
        SubjectId = subjectId;
    }

    public int getSubjectStatus() {
        return subjectStatus;
    }

    public void setSubjectStatus(int subjectStatus) {
        this.subjectStatus = subjectStatus;
    }

    public int getSubjectParentId() {
        return subjectParentId;
    }

    public void setSubjectParentId(int subjectParentId) {
        this.subjectParentId = subjectParentId;
    }

    public String getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(String totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public String getTestTotalMarks() {
        return testTotalMarks;
    }

    public void setTestTotalMarks(String testTotalMarks) {
        this.testTotalMarks = testTotalMarks;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
}
