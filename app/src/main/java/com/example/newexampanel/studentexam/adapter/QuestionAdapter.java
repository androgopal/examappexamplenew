package com.example.newexampanel.studentexam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.newexampanel.Constant;
import com.example.newexampanel.R;
import com.example.newexampanel.database.Entity.QuestionTable;
import com.example.newexampanel.databinding.ItemRowQuestionLayoutBinding;
import com.example.newexampanel.studentexam.ExamActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.RecyclerViewHolder> {
    ItemRowQuestionLayoutBinding binding;
    Context context;
    List<QuestionTable> listItem;
    private QuestionItem questionItem;
    //private QuestionStatus questionStatus;
    private ExamActivity examActivity;

    public QuestionAdapter(Context context, List<QuestionTable> listItem, QuestionItem questionItem/*, QuestionStatus questionStatus*/, ExamActivity examActivity) {
        this.context = context;
        this.listItem = listItem;
        this.questionItem = questionItem;
        //this.questionStatus = questionStatus;
        this.examActivity = examActivity;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = ItemRowQuestionLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        RecyclerViewHolder holder = new RecyclerViewHolder(binding);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        binding.txtHeaderTitle.setText("Q. " + listItem.get(position).getQuestionHeaderHindi() + "  Q. Id : -  " + listItem.get(position).getQuestionId());

        Constant.logPrint("datadatadata", listItem.size() + " +++ " + position + " +++ " + listItem.get(position).getQuestionHeaderHindi() + "+++ " + listItem.get(position).getQuestionId());

        binding.txtHeaderTitle.setText("Q. " + listItem.get(position).getQuestionHeaderHindi() + "  Q. Id : -  " + listItem.get(position).getQuestionId());
        String QueOption = listItem.get(position).getOptionData();
        ArrayList<String> questionOption = new ArrayList<String>(Arrays.asList(QueOption.split("###OPT###")));
        Constant.logPrint("1234567", questionOption.size() + "");
        binding.txtOptionA.setText(questionOption.get(0));
        binding.txtOptionB.setText(questionOption.get(1));
        binding.txtOptionC.setText(questionOption.get(2));
        binding.txtOptionD.setText(questionOption.get(3));
        if (questionOption.size() == 4) {
            binding.optCardE.setVisibility(View.GONE);
        } else if (questionOption.size() >= 5) {
            binding.optCardE.setVisibility(View.VISIBLE);
            binding.txtOptionE.setText(questionOption.get(4));
        }
        //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), i);

        Constant.quePosition = holder.getAdapterPosition();
        if (listItem.get(position).getUserSelectedAns() == 1) {
            binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
            binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
            binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
            binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
        } else if (listItem.get(position).getUserSelectedAns() == 2) {
            binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
            binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
            binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
            binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
        } else if (listItem.get(position).getUserSelectedAns() == 3) {
            binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
            binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
            binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
            binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
        } else if (listItem.get(position).getUserSelectedAns() == 4) {
            binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
            binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
            binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
            binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
        } else if (listItem.get(position).getUserSelectedAns() == 5) {
            binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
            binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
        } else {
            binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
            binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
            binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
            binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
        }

        binding.llOptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 1) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(1);
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option A +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 1 &&
                        listItem.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.REVIEW) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(1);
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    binding.llOptionA.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option A +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(0);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.UNANSWERED);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option A +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.llOptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 2) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(2);
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()),i);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option B +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 2 &&
                        listItem.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.REVIEW) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(2);
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()),i);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option A +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(0);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.UNANSWERED);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option B +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.llOptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 3) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(3);
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()),i);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option C +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 3 &&
                        listItem.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.REVIEW) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(3);
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()),i);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option A +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(0);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.UNANSWERED);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option C +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.llOptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 4) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(4);
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()),i);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option D +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 4 &&
                        listItem.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.REVIEW) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(4);
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()),i);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.green_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option D +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(0);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.UNANSWERED);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option D +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.llOptionE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 5) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(5);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()),i);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                    Toast.makeText(context, "Clicked Option E +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                } else if (listItem.get(holder.getAdapterPosition()).getUserSelectedAns() == 0 || listItem.get(holder.getAdapterPosition()).getUserSelectedAns() != 5 &&
                        listItem.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.REVIEW) {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(5);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.ANSWERED);
                    //questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()),i);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.correct_answer_layout);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.green_circle);
                    Toast.makeText(context, "Clicked Option E +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();

                } else {
                    listItem.get(holder.getAdapterPosition()).setUserSelectedAns(0);
                    questionItem.onClickQuestionItem(listItem.get(holder.getAdapterPosition()), holder.getAdapterPosition());
                    listItem.get(holder.getAdapterPosition()).setUserQueStatus(Constant.UNANSWERED);
                    binding.llOptionA.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionB.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionC.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionD.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llOptionE.setBackgroundResource(R.drawable.white_layout_background);
                    binding.llIndexA.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexB.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexC.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexD.setBackgroundResource(R.drawable.grey_circle);
                    binding.llIndexE.setBackgroundResource(R.drawable.grey_circle);
                    Toast.makeText(context, "Clicked Option E +++ " + listItem.get(holder.getAdapterPosition()).getUserSelectedAns(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.setIsRecyclable(false);
    }


    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public RecyclerViewHolder(@NonNull ItemRowQuestionLayoutBinding itemView) {
            super(itemView.getRoot());
        }
    }

    public interface QuestionItem {
        void onClickQuestionItem(QuestionTable clickedItem, int position);
    }

    public interface QuestionStatus {
        void onCheckQuestionStatus(QuestionTable question, int position);
    }

    public class ViewHolder {
    }
}
