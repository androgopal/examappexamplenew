package com.example.newexampanel.studentexam;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.newexampanel.Constant;
import com.example.newexampanel.database.Entity.QuestionTable;
import com.example.newexampanel.database.Entity.RawExamInfoTable;
import com.example.newexampanel.database.Entity.SubjectDataTable;
import com.example.newexampanel.databinding.ActivityResultBinding;
import com.example.newexampanel.studentexam.adapter.QuestionAdapter;
import com.example.newexampanel.viewmodel.model.Viewmodel;

import java.util.List;


public class ResultActivity extends AppCompatActivity {
    ActivityResultBinding binding;
    private Activity activity;
    List<QuestionTable> questionTables;
    int correct_count = 0;
    int wrong_count = 0;
    int unans_count = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityResultBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.setStatusBar(ResultActivity.this);
        activity = this;
        binding.back.setOnClickListener(v -> {
            finish();
        });

        for (int result=0; result<=questionTables.size(); result++){
            int c_c = 0, w_c = 0, u_c = 0;
            if (questionTables.get(result).getUserSelectedAns() == questionTables.get(result).getCorrectOption()) {
                correct_count = c_c++;
            } else if (questionTables.get(result).getUserSelectedAns() > 0 && questionTables.get(result).getUserSelectedAns() != questionTables.get(result).getCorrectOption()) {
                wrong_count = w_c++;
            } else {
                unans_count = u_c++;
            }Constant.logPrint("testYourOptions", correct_count + "++" + wrong_count + "++" + unans_count);
        }

    }
}