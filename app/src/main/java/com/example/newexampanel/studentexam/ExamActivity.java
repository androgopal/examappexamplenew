package com.example.newexampanel.studentexam;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.newexampanel.Constant;
import com.example.newexampanel.R;
import com.example.newexampanel.database.Entity.QuestionTable;
import com.example.newexampanel.database.Entity.RawExamInfoTable;
import com.example.newexampanel.database.Entity.SubjectDataTable;
import com.example.newexampanel.databinding.ActivityExamBinding;
import com.example.newexampanel.databinding.ItemRowQuestionLayoutBinding;
import com.example.newexampanel.studentexam.adapter.PaperAdapter;
import com.example.newexampanel.studentexam.adapter.QuestionAdapter;
import com.example.newexampanel.viewmodel.model.Viewmodel;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ExamActivity extends AppCompatActivity implements QuestionAdapter.QuestionItem/*, QuestionAdapter.QuestionStatus*/ {
    static ActivityExamBinding binding;
    static Viewmodel mViewModel;
    private Activity activity;
    String examTime = "";
    private static int quesId;
    private static int queNumPosition;
    private QuestionAdapter.QuestionItem questionItem;
    static QuestionAdapter questionAdapter;
    private List<QuestionTable> questionTable1;
    int correct_count = 0;
    int wrong_count = 0;
    int unans_count = 0;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExamBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        activity = this;
        questionItem = this;
        mViewModel = new ViewModelProvider(this).get(Viewmodel.class);
        Constant.setStatusBar(ExamActivity.this);


        // recycler scroll
        setSnapHelper();
        binding.imgMenu.setOnClickListener(view -> {
            if (!binding.drawelayout.isDrawerOpen(GravityCompat.END))
                binding.drawelayout.openDrawer(Gravity.END);
            else binding.drawelayout.closeDrawer(Gravity.END);
        });
        binding.time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mViewModel.GetExamDataFromApi(readJSONDataFromFile());
                } catch (IOException e) {
                    Toast.makeText(activity, "Invalid Json", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
        binding.totalQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewModel.DeleteAll();
            }
        });

        binding.llPay.setOnClickListener(v -> {
            exit_exam_box();
        });
        binding.submittest.setOnClickListener(v -> {
            submit_Box();
        });
        binding.imgLang.setOnClickListener(v -> {
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(ExamActivity.this);
            bottomSheetDialog.setContentView(R.layout.lang_layout);
            bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            TextView tv_english = bottomSheetDialog.findViewById(R.id.tv_english);
            TextView tv_hindi = bottomSheetDialog.findViewById(R.id.tv_hindi);
            TextView tv_Cancle = bottomSheetDialog.findViewById(R.id.tv_Cancle);
            bottomSheetDialog.show();
            tv_Cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomSheetDialog.dismiss();
                }
            });
        });
        mViewModel.getExamData().observe(this, new Observer<RawExamInfoTable>() {
            @Override
            public void onChanged(RawExamInfoTable rawExamInfoTable) {

                if (rawExamInfoTable != null) {
                    // Log.d("liveData", rawExamInfoTable.getSubId() + "");
                    binding.txtTitle.setText(rawExamInfoTable.getTestTitle());
                    binding.totalQ.setText("Total Q: " + rawExamInfoTable.getSubQuestion());
                    binding.negativeMarks.setText("-" + rawExamInfoTable.getNegativeMarks());
                    binding.totalMarks.setText("+" + rawExamInfoTable.getTestMarks());
                    examTime = rawExamInfoTable.getTestDuration();
                    startTimer();
                    mViewModel.getSubjectata(rawExamInfoTable.getTestId()).observe((ExamActivity) activity, new Observer<List<SubjectDataTable>>() {
                        @Override
                        public void onChanged(List<SubjectDataTable> subjectDataTables) {

                            if (subjectDataTables != null && subjectDataTables.size() > 0) {
                                Log.d("liveData", subjectDataTables.get(0).getSubjectName() + "" + subjectDataTables.size());


                                mViewModel.getQuestiontata(rawExamInfoTable.getSubId()).observe((ExamActivity) activity, new Observer<List<QuestionTable>>() {
                                    @Override
                                    public void onChanged(List<QuestionTable> questionTables) {
                                        if (questionTables != null) {
                                            //Log.d("liveData1", questionTables.size()+"++"+questionTables.get(1).getUserQueStatus());
                                            Log.d("liveData1", questionTables.get(1).getUserQueStatus()+"++++"+questionTables.get(1).getUserSelectedAns()+"+++"+questionTables.get(1));
                                            /*for(int i=0; i<=questionTables.size(); i++) {
                                                Log.d("values : ", questionTables.get(i).getQuestionHeaderHindi() +" +++ "+i+ questionTables.get(i).getUserQueStatus());
                                            }*/


                                            LinearLayoutManager lm = new LinearLayoutManager(getApplicationContext());
                                            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
                                            binding.questionRecycler.setLayoutManager(lm);
                                            questionAdapter = new QuestionAdapter(getApplicationContext(), questionTables, questionItem /*,questionStatus*/, ExamActivity.this);
                                            binding.questionRecycler.setAdapter(questionAdapter);

                                            binding.previous.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (quesId > 0) {
                                                        if (questionTables.get(quesId).getUserSelectedAns() == 0) {
                                                            questionTables.get(quesId).setUserQueStatus(Constant.UNANSWERED);
                                                            mViewModel.updateQuestionTable(questionTables.get(quesId));
                                                            binding.questionRecycler.smoothScrollToPosition(quesId - 1);
                                                        } else {
                                                            binding.questionRecycler.smoothScrollToPosition(quesId - 1);
                                                        }
                                                    }
                                                }
                                            });
                                            binding.next.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (quesId < questionTables.size() - 1) {
                                                        //binding.questionRecycler.smoothScrollToPosition(quesId + 1);
                                                        if (questionTables.get(quesId).getUserSelectedAns() == 0) {
                                                            questionTables.get(quesId).setUserQueStatus(Constant.UNANSWERED);
                                                            mViewModel.updateQuestionTable(questionTables.get(quesId));
                                                            binding.questionRecycler.smoothScrollToPosition(quesId + 1);
                                                        } else {
                                                            binding.questionRecycler.smoothScrollToPosition(quesId + 1);
                                                        }
                                                    }
                                                }
                                            });

                                            binding.llReview.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    binding.llReview.setVisibility(View.GONE);
                                                    binding.llReview1.setVisibility(View.VISIBLE);
                                                    //questionTables.get(finalI).setUserQueReview(true);
                                                    questionTables.get(quesId).setUserQueStatus(Constant.REVIEW);
                                                    mViewModel.updateQuestionTable(questionTables.get(quesId));
                                                    //Toast.makeText(getApplicationContext(), "Que in Review +++ " + questionTables.get(holder.getAdapterPosition()).getUserQueStatus(), Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                            binding.llReview1.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    binding.llReview1.setVisibility(View.GONE);
                                                    binding.llReview.setVisibility(View.VISIBLE);
                                                    if (questionTables.get(quesId).getUserSelectedAns() > 0) {
                                                        questionTables.get(quesId).setUserQueStatus(Constant.ANSWERED);
                                                        mViewModel.updateQuestionTable(questionTables.get(Constant.quePosition));
                                                    } else {
                                                        questionTables.get(quesId).setUserQueStatus(Constant.UNANSWERED);
                                                        mViewModel.updateQuestionTable(questionTables.get(quesId));
                                                    }
                                                    //Toast.makeText(context, "Que out Review +++ " + questionTables.get(holder.getAdapterPosition()).getUserQueStatus(), Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                            if (questionTables.get(quesId).getUserQueStatus() == Constant.REVIEW) {
                                                binding.llReview.setVisibility(View.GONE);
                                                binding.llReview1.setVisibility(View.VISIBLE);
                                            } else if (questionTables.get(quesId).getUserQueStatus() != Constant.REVIEW) {
                                                binding.llReview1.setVisibility(View.GONE);
                                                binding.llReview.setVisibility(View.VISIBLE);
                                            }

                                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                                            binding.dialogRecycler.setLayoutManager(linearLayoutManager);
                                            PaperAdapter paperAdapter = new PaperAdapter(getApplicationContext(), subjectDataTables, questionTables);
                                            binding.dialogRecycler.setAdapter(paperAdapter);


                                        }
                                        binding.clear.setOnClickListener(v -> {
                                            int c_c = 0, w_c = 0, u_c = 0;
                                            for (int j = 0; j <= questionTables.size(); j++) {
                                                int seAns = questionTables.get(j).getUserSelectedAns();
                                                int corrAns = questionTables.get(j).getCorrectOption();
                                                if (seAns == corrAns) {
                                                    correct_count = c_c++;
                                                } else if (seAns > 0 && seAns != corrAns) {
                                                    wrong_count = w_c++;
                                                } else {
                                                    unans_count = u_c++;
                                                }
                                                Constant.logPrint("testYourOptions", correct_count + "++" + wrong_count + "++" + unans_count);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    private void exit_exam_box() {
        final Dialog dialog = new Dialog(ExamActivity.this);
        dialog.setContentView(R.layout.exit_exam_box);
        TextView exam_cancle = dialog.findViewById(R.id.exam_cancle);
        TextView exam_submit = dialog.findViewById(R.id.exam_submit);
        exam_submit.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
            startActivity(intent);
            dialog.dismiss();
        });
        dialog.show();
        exam_cancle.setOnClickListener(v -> {
            dialog.dismiss();
        });
    }

    @SuppressLint("WrongConstant")
    private void submit_Box() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.test_submit_box, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Button btn_cancle = dialogView.findViewById(R.id.btn_cancle);
        Button btn_submit = dialogView.findViewById(R.id.btn_submit);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        btn_submit.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
            startActivity(intent);
            alertDialog.dismiss();
        });
        btn_cancle.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }

    public String readJSONDataFromFile() throws IOException {
        InputStream inputStream = null;
        StringBuilder builder = new StringBuilder();

        try {

            String jsonString = null;
            inputStream = getResources().openRawResource(R.raw.exam_data);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
                //Constant.logPrint("fjfjfjfffjfjf", jsonString + "");
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }

    @Override
    public void onClickQuestionItem(QuestionTable clickedItem, int position) {
        mViewModel.updateQuestionTable(clickedItem);

    }



/*    @Override
    public void checkQuestionStatus(QuestionTable clickedItem, int position) {

    }*/

/*    // PopularCoursesAdapter
    public class PaperAdapter extends RecyclerView.Adapter<PaperAdapter.ViewHolder> {
        List<SubjectDataTable> subjectDataTables;
        List<QuestionTable> questionTables;
        private Context mContext;

        public PaperAdapter(Context context, List<SubjectDataTable> subjectDataTables, List<QuestionTable> questionTables) {
            this.mContext = context;
            this.subjectDataTables = subjectDataTables;
            this.questionTables = questionTables;

        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_menu_ques, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.txt_ques.setText(subjectDataTables.get(position).getSubjectName());
            holder.im_arrow_down.setOnClickListener(v -> {
                holder.im_arrrow_up.setVisibility(View.VISIBLE);
                holder.sub_recyler.setVisibility(View.GONE);
                holder.im_arrow_down.setVisibility(View.GONE);
            });
            holder.im_arrrow_up.setOnClickListener(v -> {
                holder.im_arrow_down.setVisibility(View.VISIBLE);
                holder.sub_recyler.setVisibility(View.VISIBLE);
                holder.im_arrrow_up.setVisibility(View.GONE);
            });
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return subjectDataTables.size();

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView im_arrow_down, im_arrrow_up;
            TextView txt_ques;
            RecyclerView sub_recyler;
            LinearLayout sub_layout;

            public ViewHolder(@NonNull View view) {
                super(view);
                sub_layout = view.findViewById(R.id.sub_layout);
                txt_ques = view.findViewById(R.id.txt_ques);
                im_arrow_down = view.findViewById(R.id.im_arrow_down);
                im_arrrow_up = view.findViewById(R.id.im_arrrow_up);
                sub_recyler = view.findViewById(R.id.sub_recyler);
                GridLayoutManager lm = new GridLayoutManager(mContext, 5);
                sub_recyler.setLayoutManager(lm);

                ExamListAdapter examListAdapter = new ExamListAdapter(mContext, questionTables);
                sub_recyler.setAdapter(examListAdapter);
            }
        }
    }*/

    // PopularCoursesAdapter


    public static class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.ViewHolder> {
        Context context;
        List<QuestionTable> questionTables;
        private QuestionAdapter.QuestionItem questionItem;

        public ExamListAdapter(Context context, List<QuestionTable> questionTables, QuestionAdapter.QuestionItem questionItem) {
            this.context = context;
            this.questionTables = questionTables;
            this.questionItem = questionItem;
            //this.answerLists=answerLists;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.exam_list_layout, viewGroup, false);
            return new ViewHolder(view);
        }

        @SuppressLint({"WrongConstant", "LongLogTag", "ResourceAsColor"})
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
            int finalPosition1 = i;

            if (questionTables.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.ANSWERED) {
                holder.exam_ques.setBackgroundResource(R.drawable.answered_circle);
                holder.exam_ques.setTextColor(Color.WHITE);
            } else if (questionTables.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.REVIEW) {
                holder.exam_ques.setBackgroundResource(R.drawable.review_cricle);
                holder.exam_ques.setTextColor(Color.WHITE);
            } else if (questionTables.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.UNANSWERED) {
                holder.exam_ques.setBackgroundResource(R.drawable.not_answered_circle);
                holder.exam_ques.setTextColor(Color.WHITE);
            } else if (questionTables.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.NOT_VISITED) {
                holder.exam_ques.setBackgroundResource(R.drawable.grey_border);
                holder.exam_ques.setTextColor(Color.BLACK);
            }
            int finalI = i;

            int d = ++i;
            holder.exam_ques.setText(d + "");
            //Constant.logPrint("dposition", d + "");
            int finalPosition = i;

            holder.ll_exam_ques.setOnClickListener(v -> {
                questionAdapter.notifyDataSetChanged();
                if (questionTables.get(quesId).getUserQueStatus() == 0) {
                    questionTables.get(quesId).setUserQueStatus(Constant.UNANSWERED);
                    mViewModel.updateQuestionTable(questionTables.get(quesId));
                    binding.questionRecycler.scrollToPosition(finalPosition - 1);
                } else {
                    binding.questionRecycler.scrollToPosition(finalPosition - 1);
                }
                if (binding.drawelayout.isDrawerOpen(Gravity.END)) {
                    binding.drawelayout.closeDrawer(Gravity.END);
                } else {
                    binding.drawelayout.openDrawer(Gravity.END);
                }
            });

        }

        @Override
        public int getItemCount() {
            return questionTables.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView exam_ques;
            LinearLayout ll_exam_ques;

            public ViewHolder(@NonNull View view) {
                super(view);
                ll_exam_ques = view.findViewById(R.id.ll_exam_ques);
                exam_ques = view.findViewById(R.id.exam_ques);
            }
        }


    }

    private void setSnapHelper() {
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(binding.questionRecycler);

        binding.questionRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                View view = snapHelper.findSnapView(recyclerView.getLayoutManager());
                quesId = recyclerView.getLayoutManager().getPosition(view);
                binding.tvquestionNo.setText("Q." + String.valueOf(quesId + 1));
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }


    private void startTimer() {
        long totalTime = Long.parseLong(examTime) * 60 * 1000;
        CountDownTimer timer = new CountDownTimer(totalTime + 1000, 1000) {
            @Override
            public void onTick(long remainingTime) {
                String time = String.format("%02d:%02d min",
                        TimeUnit.MILLISECONDS.toMinutes(remainingTime),
                        TimeUnit.MILLISECONDS.toSeconds(remainingTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime))
                );
                binding.time.setText(time);
            }

            @Override
            public void onFinish() {

            }
        };
        timer.start();
    }


   /* @Override
    public void onBackPressed() {

    }*/
}