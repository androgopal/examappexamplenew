package com.example.newexampanel.studentexam.adapter;

import com.example.newexampanel.database.Entity.QuestionTable;

public interface ReviewInterface {
    void onReviewClick(QuestionTable question, int position);
}
