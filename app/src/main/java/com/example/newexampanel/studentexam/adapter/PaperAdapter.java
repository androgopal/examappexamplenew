package com.example.newexampanel.studentexam.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newexampanel.R;
import com.example.newexampanel.database.Entity.QuestionTable;
import com.example.newexampanel.database.Entity.SubjectDataTable;
import com.example.newexampanel.studentexam.ExamActivity;

import java.util.List;

// PopularCoursesAdapter
public class PaperAdapter extends RecyclerView.Adapter<PaperAdapter.ViewHolder> {
    List<SubjectDataTable> subjectDataTables;
    List<QuestionTable> questionTables;
    QuestionAdapter.QuestionItem questionItem;
    private Context mContext;

    public PaperAdapter(Context context, List<SubjectDataTable> subjectDataTables, List<QuestionTable> questionTables) {
        this.mContext = context;
        this.subjectDataTables = subjectDataTables;
        this.questionTables = questionTables;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_menu_ques, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txt_ques.setText(subjectDataTables.get(position).getSubjectName());
        holder.im_arrow_down.setOnClickListener(v -> {
            holder.im_arrrow_up.setVisibility(View.VISIBLE);
            holder.sub_recyler.setVisibility(View.GONE);
            holder.im_arrow_down.setVisibility(View.GONE);
        });
        holder.im_arrrow_up.setOnClickListener(v -> {
            holder.im_arrow_down.setVisibility(View.VISIBLE);
            holder.sub_recyler.setVisibility(View.VISIBLE);
            holder.im_arrrow_up.setVisibility(View.GONE);
        });
        Log.d("helloloo", subjectDataTables.size()+"");
        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return subjectDataTables.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView im_arrow_down, im_arrrow_up;
        TextView txt_ques;
        RecyclerView sub_recyler;
        LinearLayout sub_layout;

        public ViewHolder(@NonNull View view) {
            super(view);
            sub_layout = view.findViewById(R.id.sub_layout);
            txt_ques = view.findViewById(R.id.txt_ques);
            im_arrow_down = view.findViewById(R.id.im_arrow_down);
            im_arrrow_up = view.findViewById(R.id.im_arrrow_up);
            sub_recyler = view.findViewById(R.id.sub_recyler);
            GridLayoutManager lm = new GridLayoutManager(mContext, 5);
            sub_recyler.setLayoutManager(lm);

            ExamActivity.ExamListAdapter examListAdapter = new ExamActivity.ExamListAdapter(mContext, questionTables,questionItem);
            sub_recyler.setAdapter(examListAdapter);
        }
    }
}
