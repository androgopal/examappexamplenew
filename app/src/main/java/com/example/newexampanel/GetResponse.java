package com.example.newexampanel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetResponse {
    @Headers("Cache-Control: no-cache")
    @FormUrlEncoded
    @POST(Constant.LOGIN)
    Call<String> doLogin(
            @Field("student_email") String user_email,
            @Field("student_password") String user_password);


}
