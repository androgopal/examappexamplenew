package com.example.newexampanel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class QuestionGridAdapter extends BaseAdapter {
    private int numOfQues;
    public QuestionGridAdapter(int numOfQues){
        this.numOfQues= numOfQues;
    }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View myview;
        if (view == null){
            myview = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.ques_grid_item,parent,false);
        }else {
            myview = view;
        }

        TextView quesTV = myview.findViewById(R.id.ques_num);
        quesTV.setText(String.valueOf(position+1));

        //switch ( )

        return myview;
    }
}
